SET search_path TO {{work_schema}};

-- create a file with all metavisits where visit_type is inpatient or emergency (2003-16)
\echo 'fetching metavisits'
create temporary table bob as (
	  SELECT X1.metavisit_occurrence_id, X1.concept_ids, X1.concept_set_name, 
	         X2.visit_types, X2.metavisit_period, 
	         X3.gender, extract(year from (lower(X2.metavisit_period)))- extract(year from X3.birth_date) as age
	    FROM {{work_schema}}.events_of_interest X1, 
	         {{work_schema}}.metavisits_of_interest X2, 
	         {{work_schema}}.cohort_of_interest X3
	   WHERE X1.metavisit_occurrence_id  = X2.metavisit_occurrence_id 
	     AND X2.person_id = X3.person_id
	     AND ('inpatient' = ANY(X2.visit_types)  OR 'emergency' = ANY(X2.visit_types))
	   ORDER BY X1.metavisit_occurrence_id
	   LIMIT 100000
);
\echo 'exporting metavisits'
\copy bob to PROGRAM 'gzip > machine_learning/{{work_schema}}_metavisits.tsv.gz' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';

\echo 'exporting concept_ancestor'
\copy (select * from {{cdm_schema}}.concept_ancestor) to PROGRAM 'gzip > machine_learning/{{work_schema}}_concept_ancestor.tsv.gz' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';

\echo 'exporting concept_relationship'
\copy (select * from {{cdm_schema}}.concept_relationship) to PROGRAM 'gzip > machine_learning/{{work_schema}}_concept_relationship.tsv.gz' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';

\echo 'exporting concept'
\copy (select * from {{cdm_schema}}.concept) to PROGRAM 'gzip > machine_learning/{{work_schema}}_concept.tsv.gz' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';
