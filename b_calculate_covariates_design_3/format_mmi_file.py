#!/usr/bin/python
"""Convert the mmi database pull into a set of binary indictators to
say which mmi diagnoses are present per person_id.
"""
import sys
import gzip
if len(sys.argv) != 3:
    sys.stderr.write("Usage: " + sys.argv[0] + " <mmi_infile> <mmi_outfile>\n")
    sys.exit(1)

inpfile = sys.argv[1]
outfile = sys.argv[2]

lineNo = -1
md_set = set()
sc_set = set()
sa_set = set()
bd_set = set()
person_id_set = set()
#start processing
with gzip.open(inpfile, 'r') as fin:
    for line in fin:
        lineNo+=1
        if (lineNo == 0):
            continue
        vals = line.strip().split('\t')
        person_id = vals[0].strip(' ')
        person_id_set.add(int(person_id))
        event_start_date = vals[1].split(',')[0][1:]
        if ('diagnoses;mmi;bipolar_spectrum;major_depressive' in vals[2]):
            md_set.add(person_id)
        elif ('diagnoses;mmi;schizophrenia' in vals[2]):
            sc_set.add(person_id)
        elif ('diagnoses;mmi;bipolar_spectrum;schizoaffective' in vals[2]):
            sa_set.add(person_id)
        elif ('diagnoses;mmi;bipolar_spectrum;bipolar_disorder' in vals[2]):
            bd_set.add(person_id)
        else:
            print ('Something went wrong with the query!!!!')


hdr = "person_id\thad_bipolar_disorder\thad_major_depressive_episode\thad_schizoaffective_disorder\thad_schizophrenia\n"
ofile = gzip.open(outfile, 'w')

ofile.write(hdr)
for p in sorted(person_id_set):
    ps = str(p)
    line = ps + "\t" + str((ps in bd_set) + 0) + "\t" + str((ps in md_set) + 0) + "\t" + str((ps in sa_set) + 0) + "\t" + str((ps in sc_set) + 0) + "\n"
    ofile.write(line)

