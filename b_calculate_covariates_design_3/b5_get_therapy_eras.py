#!/usr/bin/python
"""
Convert the compressed, long-formatted table produced by `b5_get_therapy_eras.sql`
into a wide-formatted table suitable for downstream processing.
"""

import csv
import datetime
import gzip
import sys

def open_input():
    i_fn = sys.argv[1]
    if (i_fn.lower().endswith(".gz")):
        i_fh = gzip.open(i_fn, "rb")
    else:
        i_fh = open(i_fn, "r")

    return csv.DictReader(i_fh)

def to_concept_set_names(text):
    assert (text.startswith("{") and text.endswith("}"))
    for concept_set_name in text[1:-1].split(","):
        if (concept_set_name != ""):
            yield concept_set_name

to_date = lambda text: datetime.datetime.strptime(text, r"%Y-%m-%d").date()

# first pass: list concept set names
all_concept_set_names = set()
for entry in open_input():
    for concept_set_name in to_concept_set_names(entry["concept_set_names"]):
        all_concept_set_names.add(concept_set_name)

all_concept_set_names = sorted(all_concept_set_names)

# second pass: convert from long to wide
o_fh = csv.writer(sys.stdout)
o_fh.writerow(["person_id", "period_start", "period_stop"] + all_concept_set_names)

for entry in open_input():
    # extract period's start and end date
    period = entry["period"]
    assert (period.startswith("["))
    assert (period.endswith(")"))
    period_start, period_stop = [to_date(date) for date in period[1:-1].split(",")]

    row = [
        entry["person_id"],
        period_start.isoformat(),
        period_stop - datetime.timedelta(days = 1)]

    concept_set_names = set(to_concept_set_names(entry["concept_set_names"]))
    for concept_set_name in all_concept_set_names:
        if (concept_set_name in concept_set_names):
            row.append(1)
        else:
            row.append(0)

    o_fh.writerow(row)
