\echo '  calculating covariate (a)(a), `general_anesthesia_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'general_anesthesia_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;general_an;%');

\echo '  calculating covariate (a)(b), `general_anesthesia_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'general_anesthesia_during_period_2'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;general_an;%');

\echo '  calculating covariate (b)(a), `cns_surgery_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'cns_surgery_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;operat_nerv;%');

\echo '  calculating covariate (b)(b), `cns_surgery_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'cns_surgery_during_period_2'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;operat_nerv;%');

\echo '  calculating covariate (c)(a), `apnea_related_surgery_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'apnea_related_surgery_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;apnea;%');

\echo '  calculating covariate (c)(b), `apnea_related_surgery_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'apnea_related_surgery_during_period_2'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'procedures;brain_procedure;apnea;%');
