
\echo '    staging events'

\echo '      creating _events_during_period_2'

CREATE TEMPORARY TABLE _events_during_period_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN events_of_interest AS event
   USING (person_id)
   WHERE (event.concept_set_name LIKE 'diagnoses;%')
     AND (event.event_period && index_event.period_2);

CREATE INDEX ON _events_during_period_2 (person_id);
CREATE INDEX ON _events_during_period_2 (concept_set_name);
CREATE INDEX ON _events_during_period_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate (a)(i), `diagnosis_of_bipolar_i_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_i_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_i;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (a)(ii), `diagnosis_of_bipolar_ii_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_ii_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_ii;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (a)(iii), `diagnosis_of_bipolar_nos_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_nos_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_nos;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (a)(iv), `diagnosis_of_mdd_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_mdd_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;bipolar_spectrum;major_depressive;no_remission;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (a)(v), `diagnosis_of_sad_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_sad_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;bipolar_spectrum;schizoaffective;no_remission;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (a)(vi), `diagnosis_of_schizophrenia_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'diagnosis_of_schizophrenia_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE 'diagnoses;' ||
                    'mmi;schizophrenia;no_remission;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(i), `manic_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'manic_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;polarity;manic;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;depressive;%')
                        OR (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;mixed;%')
                       )
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(ii), `depressive_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'depressive_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;polarity;depressive;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;manic;%')
                        OR (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;mixed;%')
                       )
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(iii), `mixed_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'mixed_episode_during_period_2'::TEXT,
         (
             (EXISTS (
               SELECT TRUE
                 FROM _events_during_period_2 AS visit
                WHERE (visit.person_id = op_start.person_id)
                  AND (visit.concept_set_name LIKE
                        'diagnoses;episode;polarity;mixed;%')
               ))
          OR (
                  (EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;manic;%')
                    ))
              AND (EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;depressive;%')
                    ))
             )
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(iv), `unknown_polarity_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'unknown_polarity_episode_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (
                      (visit.concept_set_name LIKE
                        'diagnoses;episode;polarity;unknown;%')
                   OR (visit.concept_set_name LIKE
                        'diagnoses;mmi;schizophrenia;no_remission;%')
                  )
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(i), `mild_mood_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'mild_mood_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;severity;mild;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'diagnoses;episode;severity;moderate;%')
                        OR (visit.concept_set_name LIKE
                             'diagnoses;episode;severity;severe;%')
                       )
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(ii), `moderate_mood_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'moderate_mood_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;severity;moderate;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;severity;severe;%')
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(iii), `severe_mood_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'severe_mood_episode_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'diagnoses;episode;severity;severe;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(iv), `unknown_mood_episode_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'unknown_mood_episode_during_period_2'::TEXT,
         (
             (
                  (EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (visit.concept_set_name LIKE
                             'diagnoses;episode;polarity;unknown;%')
                    ))
              AND (NOT EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (
                               (visit.concept_set_name LIKE
                                 'diagnoses;episode;severity;mild;%')
                            OR (visit.concept_set_name LIKE
                                 'diagnoses;episode;severity;moderate;%')
                            OR (visit.concept_set_name LIKE
                                 'diagnoses;episode;severity;severe;%')
                           )
                    ))
             )
          OR (EXISTS (
               SELECT TRUE
                 FROM _events_during_period_2 AS visit
                WHERE (visit.person_id = op_start.person_id)
                  AND (visit.concept_set_name LIKE
                        'diagnoses;mmi;schizophrenia;no_remission;%')
               ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(i), `psychotic_features_present_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'psychotic_features_present_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'diagnoses;episode;psychotic;present;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(ii), `psychotic_features_absent_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'psychotic_features_absent_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;psychotic;absent;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'diagnoses;episode;psychotic;present;%')
                        OR (visit.concept_set_name LIKE
                             'diagnoses;mmi;schizophrenia;no_remission;%')
                       )
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(iii), `psychotic_features_unknown_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'psychotic_features_unknown_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'diagnoses;episode;psychotic;unknown;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'diagnoses;episode;psychotic;present;%')
                        OR (visit.concept_set_name LIKE
                             'diagnoses;episode;psychotic;absent;%')
                       )
                ))
         )::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

/**
Test: for any patient_id, each of these covariates should have a value

    SELECT variable_name,
           count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
           count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
           count(*) AS n_total
      FROM "variables,design_3"
     WHERE (variable_name LIKE '%_during_period_2')
  GROUP BY variable_name
  ORDER BY variable_name;

**/
