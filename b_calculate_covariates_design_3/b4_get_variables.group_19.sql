
\echo '    staging events'

\echo '      creating _drugs_during_periods_1_or_2'

DROP TABLE IF EXISTS _drugs_during_periods_1_or_2 CASCADE;
CREATE TEMPORARY TABLE _drugs_during_periods_1_or_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN events_of_interest AS event
   USING (person_id)
   WHERE (event.concept_set_name LIKE 'drugs;drug_of_interest;%')
     AND (event.event_period && codex_period(
            lower(index_event.period_1),
            upper(index_event.period_2)));

CREATE INDEX ON _drugs_during_periods_1_or_2 (person_id);
CREATE INDEX ON _drugs_during_periods_1_or_2 (concept_set_name);
CREATE INDEX ON _drugs_during_periods_1_or_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate (a), `exposure_to_lithium_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_lithium_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;lithium_carbonate;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b), `exposure_to_msa_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_msa_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;mood_stabilizer;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(i), `exposure_to_carbamazepine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_carbamazepine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;mood_stabilizer;carbamazepine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(ii), `exposure_to_lamotrigine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_lamotrigine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;mood_stabilizer;lamotrigine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(iii), `exposure_to_oxcarbazepine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_oxcarbazepine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;mood_stabilizer;oxcarbazepine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b)(iv), `exposure_to_valproate_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_valproate_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;mood_stabilizer;valproate;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c), `exposure_to_sga_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_sga_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(i), `exposure_to_aripiprazole_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_aripiprazole_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;aripiprazole;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(ii), `exposure_to_clozapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_clozapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;clozapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(iii), `exposure_to_asenapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_asenapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;asenapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(iv), `exposure_to_iloperidone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_iloperidone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;iloperidone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(v), `exposure_to_lurasidone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_lurasidone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;lurasidone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(vi), `exposure_to_olanzapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_olanzapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;olanzapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(vii), `exposure_to_paliperidone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_paliperidone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;paliperidone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(viii), `exposure_to_quetiapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_quetiapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;quetiapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(ix), `exposure_to_risperidone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_risperidone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;risperidone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c)(x), `exposure_to_ziprasidone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_ziprasidone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;second_generation_antipsychotic;ziprasidone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d), `exposure_to_fga_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_fga_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(i), `exposure_to_chlorpromazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_chlorpromazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;chlorpromazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(ii), `exposure_to_chlorprothixene_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_chlorprothixene_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;chlorprothixene;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(iii), `exposure_to_fluphenazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_fluphenazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;fluphenazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(iv), `exposure_to_haloperidol_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_haloperidol_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;haloperidol;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(v), `exposure_to_loxapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_loxapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;loxapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(vi), `exposure_to_mesoridazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_mesoridazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;mesoridazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(vii), `exposure_to_methotrimeprazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_methotrimeprazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;methotrimeprazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(viii), `exposure_to_molindone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_molindone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;molindone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(ix), `exposure_to_perazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_perazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;perazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(x), `exposure_to_perphenazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_perphenazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;perphenazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xii), `exposure_to_pimozide_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_pimozide_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;pimozide;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xii), `exposure_to_prochlorperazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_prochlorperazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;prochlorperazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xiii), `exposure_to_promazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_promazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;promazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xiv), `exposure_to_thioridazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_thioridazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;thioridazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xv), `exposure_to_thiothixene_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_thiothixene_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;thiothixene;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xvi), `exposure_to_trifluoperazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_trifluoperazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;trifluoperazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d)(xvii), `exposure_to_triflupromazine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_triflupromazine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;first_generation_antipsychotic;triflupromazine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e), `exposure_to_mao_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_mao_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;mao;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(i), `exposure_to_isocarboxazid_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_isocarboxazid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;mao;isocarboxazid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(ii), `exposure_to_phenelzine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_phenelzine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;mao;phenelzine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(iii), `exposure_to_selegiline_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_selegiline_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;mao;selegiline;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(iv), `exposure_to_tranylcypromine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_tranylcypromine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;mao;tranylcypromine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (f), `exposure_to_nassa_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_nassa_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;nassa;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (f)(i), `exposure_to_mirtazapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_mirtazapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;nassa;mirtazapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (g), `exposure_to_ndri_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_ndri_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ndri;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (g)(i), `exposure_to_bupropion_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_bupropion_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ndri;bupropion;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h), `exposure_to_snri_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_snri_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h)(i), `exposure_to_desvenlafaxine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_desvenlafaxine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;desvenlafaxine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h)(ii), `exposure_to_duloxetine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_duloxetine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;duloxetine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h)(iii), `exposure_to_levomilnacipran_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_levomilnacipran_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;levomilnacipran;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h)(iv), `exposure_to_milnacipran_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_milnacipran_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;milnacipran;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h)(v), `exposure_to_venlafaxine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_venlafaxine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;snri;venlafaxine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i), `exposure_to_ssri_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_ssri_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(i), `exposure_to_citalopram_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_citalopram_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;citalopram;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(ii), `exposure_to_escitalopram_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_escitalopram_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;escitalopram;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(iii), `exposure_to_fluoxetine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_fluoxetine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;fluoxetine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(iv), `exposure_to_fluvoxamine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_fluvoxamine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;fluvoxamine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(v), `exposure_to_paroxetine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_paroxetine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;paroxetine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(vi), `exposure_to_sertraline_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_sertraline_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;sertraline;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(vii), `exposure_to_vilazodone_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_vilazodone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;vilazodone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i)(viii), `exposure_to_vortioxetine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_vortioxetine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;ssri;vortioxetine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j), `exposure_to_tri_or_tetracyclic_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_tri_or_tetracyclic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(i), `exposure_to_amoxapine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_amoxapine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;amoxapine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(ii), `exposure_to_clomipramine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_clomipramine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;clomipramine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(iii), `exposure_to_desipramine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_desipramine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;desipramine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(iv), `exposure_to_doxepin_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_doxepin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;doxepin;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(v), `exposure_to_imipramine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_imipramine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;imipramine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(vi), `exposure_to_maprotiline_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_maprotiline_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;maprotiline;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(vii), `exposure_to_protriptyline_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_protriptyline_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;protriptyline;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j)(viii), `exposure_to_trimipramine_during_period_1_or_2`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_trimipramine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_or_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;trimipramine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

/**
Test: for any patient_id, each of these covariates should have a value

  SELECT variable_name,
         count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
         count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
         count(*) AS n_total
    FROM "variables,design_3"
   WHERE (variable_name LIKE 'exposure_to_%_during_period_1_or_2')
GROUP BY variable_name
ORDER BY variable_name;
**/
