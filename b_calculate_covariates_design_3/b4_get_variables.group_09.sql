
\echo '    staging events'

\echo '      creating _drugs_during_period_1_to_3'

DROP TABLE IF EXISTS _drugs_during_period_1_to_3 CASCADE;
CREATE TEMPORARY TABLE _drugs_during_period_1_to_3 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN events_of_interest AS event
   USING (person_id)
   WHERE (event.concept_set_name LIKE 'drugs;concomitant;%')
     AND (event.event_period && codex_period(
            lower(index_event.period_1),
            index_event.period_3));

CREATE INDEX ON _drugs_during_period_1_to_3 (person_id);
CREATE INDEX ON _drugs_during_period_1_to_3 (concept_set_name);
CREATE INDEX ON _drugs_during_period_1_to_3 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate (a), `exposure_to_opioid_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_opioid_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;analgesic;opioid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (b), `exposure_to_nsaid_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_nsaid_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;nsaid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (c), `exposure_to_non_narcotic_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_non_narcotic_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;analgesic;non_narcotic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (d), `exposure_to_sedative_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_sedative_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;hypnotic_sedativ;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(a), `exposure_to_antianxiety_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_antianxiety_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_anxiety;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (e)(b), `exposure_to_benzodiazepine_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_benzodiazepine_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;benzodiazepine;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (f), `exposure_to_anticonvulsant_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_anticonvulsant_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_convulsant;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (g), `exposure_to_antibacterial_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_antibacterial_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (h), `exposure_to_antiviral_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_antiviral_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_viral;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (i), `exposure_to_anti_hiv_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_anti_hiv_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_hiv;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (j), `exposure_to_antitubercular_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_antitubercular_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_tubercular;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (k), `exposure_to_ace_inhibitor_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_ace_inhibitor_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;ace_inhibitor;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (l)(a), `exposure_to_diuretic_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_diuretic_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;diuretic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (l)(b), `exposure_to_loop_diuretic_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_loop_diuretic_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;loop_diuretic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (l)(c), `exposure_to_thiazide_diuretic_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_thiazide_diuretic_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;thiazid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (m), `exposure_to_beta_blocker_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_beta_blocker_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;beta_blocker;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (n), `exposure_to_anti_arrhythmia_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_anti_arrhythmia_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_arrhytmic;%') -- note the typo here
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (o), `exposure_to_calcium_channel_blocker_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_calcium_channel_blocker_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;calcium_channel;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (p), `exposure_to_cns_stimulant_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_cns_stimulant_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;cns_stimulant;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (q), `exposure_to_alcohol_deterrent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_alcohol_deterrent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;alcohol_deterrent;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (r), `exposure_to_narcotic_antagonist_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_narcotic_antagonist_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;narcotic_antagonist;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(a), `exposure_to_hormone_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_hormone_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;hormone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(b), `exposure_to_androgen_antagonist_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_androgen_antagonist_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;androgen_antagonist;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(c), `exposure_to_estrogen_antagonist_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_estrogen_antagonist_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen_antagonist;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(d), `exposure_to_estrogen_receptor_modulator_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_estrogen_receptor_modulator_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen_receptor_modulator;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(e), `exposure_to_prostaglandin_antagonist_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_prostaglandin_antagonist_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;prostaglandin_antagonist;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(f), `exposure_to_androgen_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_androgen_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;androgen;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(g), `exposure_to_estrogen_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_estrogen_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(h), `exposure_to_progestin_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_progestin_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;progestin;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(i), `exposure_to_reproductive_control_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_reproductive_control_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;reproductive_control;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(j), `exposure_to_glucocorticoid_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_glucocorticoid_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;glucocorticoid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(k), `exposure_to_thyroid_hormone_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_thyroid_hormone_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;thyroid_hormone;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (s)(l), `exposure_to_anti_thyroid_agent_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_anti_thyroid_agent_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_thyroid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '    calculating covariate (t), `exposure_to_p450_inducer_or_inhibitor_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'exposure_to_p450_inducer_or_inhibitor_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_period_1_to_3 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (
                      (drug_exposure.concept_set_name LIKE
                        'drugs;concomitant;cytochrome_p450_inducer;%')
                   OR (drug_exposure.concept_set_name LIKE
                        'drugs;concomitant;cytochrome_p450_inhibitor;%')
                  )
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

/**
Test: for any patient_id, each of these covariates should have a value

    SELECT variable_name,
           count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
           count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
           count(*) AS n_total
      FROM "variables,design_3"
     WHERE (variable_name LIKE 'exposure_to_%_during_period_1_to_3')
  GROUP BY variable_name
  ORDER BY variable_name;

**/
