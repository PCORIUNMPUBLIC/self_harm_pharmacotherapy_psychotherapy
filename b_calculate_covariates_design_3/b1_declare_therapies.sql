-- STEP 1: Declare therapies of interest

SET search_path TO {{work_schema}};

\echo '* declaring therapies of interest'

DROP TABLE IF EXISTS "therapies_of_interest,design_3" CASCADE;
CREATE TABLE "therapies_of_interest,design_3"
  (
        therapy_name TEXT NOT NULL,
   concept_set_names TEXT[] NOT NULL
  );

COMMENT ON TABLE "therapies_of_interest,design_3" IS '{{pipeline_version}}';

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'lithium_carbonate', ARRAY[
    'drugs;drug_of_interest;lithium_carbonate;%']);

-- Mood stabilizing anticonvulsants (MSA)
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'carbamazepine', ARRAY[
    'drugs;drug_of_interest;mood_stabilizer;carbamazepine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'lamotrigine', ARRAY[
    'drugs;drug_of_interest;mood_stabilizer;lamotrigine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'oxcarbazepine', ARRAY[
    'drugs;drug_of_interest;mood_stabilizer;oxcarbazepine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'valproate', ARRAY[
    'drugs;drug_of_interest;mood_stabilizer;valproate;%']);

-- Second generation antipsychotics (SGA)
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'aripiprazole', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;aripiprazole;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'asenapine', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;asenapine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'iloperidone', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;iloperidone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'lurasidone', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;lurasidone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'olanzapine', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;olanzapine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'paliperidone', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;paliperidone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'quetiapine', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;quetiapine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'risperidone', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;risperidone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'ziprasidone', ARRAY[
    'drugs;drug_of_interest;second_generation_antipsychotic;ziprasidone;%']);

-- First generation antipsychotics
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'chlorpromazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;chlorpromazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'chlorprothixene', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;chlorprothixene;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'fluphenazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;fluphenazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'haloperidol', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;haloperidol;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'loxapine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;loxapine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'mesoridazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;mesoridazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'methotrimeprazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;methotrimeprazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'molindone', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;molindone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'perazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;perazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'perphenazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;perphenazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'pimozide', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;pimozide;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'promazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;promazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'thioridazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;thioridazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'thiothixene', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;thiothixene;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'trifluoperazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;trifluoperazine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'triflupromazine', ARRAY[
    'drugs;drug_of_interest;first_generation_antipsychotic;triflupromazine;%']);

-- MAO inhibitors antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'isocarboxazid', ARRAY[
    'drugs;drug_of_interest;anti_depressant;mao;isocarboxazid;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'phenelzine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;mao;phenelzine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'selegiline', ARRAY[
    'drugs;drug_of_interest;anti_depressant;mao;selegiline;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'tranylcypromine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;mao;tranylcypromine;%']);

-- NASSA antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'mirtazapine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;nassa;mirtazapine;%']);

-- NDRI antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'bupropion', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ndri;bupropion;%']);

-- SNRI antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'desvenlafaxine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;snri;desvenlafaxine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'duloxetine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;snri;duloxetine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'levomilnacipran', ARRAY[
    'drugs;drug_of_interest;anti_depressant;snri;levomilnacipran;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'milnacipran', ARRAY[
    'drugs;drug_of_interest;anti_depressant;snri;milnacipran;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'venlafaxine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;snri;venlafaxine;%']);

-- SSRI antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'citalopram', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;citalopram;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'escitalopram', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;escitalopram;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'fluoxetine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;fluoxetine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'fluvoxamine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;fluvoxamine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'paroxetine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;paroxetine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'sertraline', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;sertraline;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'vilazodone', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;vilazodone;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'vortioxetine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;ssri;vortioxetine;%']);

-- Tri and tetracyclic antidepressants
INSERT INTO "therapies_of_interest,design_3" VALUES (
  'amoxapine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;amoxapine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'clomipramine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;clomipramine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'desipramine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;desipramine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'doxepin', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;doxepin;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'imipramine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;imipramine;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'maprotiline', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;maprotiline;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'protriptyline', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;protriptyline;%']);

INSERT INTO "therapies_of_interest,design_3" VALUES (
  'trimipramine', ARRAY[
    'drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;trimipramine;%']);

/**
Test: each therapy should link to a non-zero number of concepts

    SELECT therapy.therapy_name,
           array_length(therapy.concept_set_names, 1) AS n_concept_sets,
           count(concept_set.concept_id) AS n_concepts
      FROM concept_sets_of_interest AS concept_set
      JOIN "therapies_of_interest,design_3" AS therapy
        ON (concept_set.concept_set_name LIKE ANY (therapy.concept_set_names))
  GROUP BY therapy.therapy_name,
           therapy.concept_set_names
  ORDER BY n_concept_sets ASC;
**/
