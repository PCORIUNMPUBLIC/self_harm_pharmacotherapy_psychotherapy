
\echo '    staging events'

CREATE TEMPORARY TABLE _visits_during_period_2 AS
  SELECT visit.*
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN visits_of_interest AS visit
   USING (person_id)
   WHERE (visit.visit_period && codex_period(
            lower(index_event.period_1),
            index_event.period_3));

CREATE INDEX ON _visits_during_period_2 (person_id);
CREATE INDEX ON _visits_during_period_2 (visit_type);

\echo '    calculating covariate (a), `had_outpatient_visit_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'had_outpatient_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _visits_during_period_2 AS visit
            WHERE (visit.person_id = index_event.person_id)
              AND (visit.visit_type = 'outpatient')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

\echo '    calculating covariate (b), `had_er_visit_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'had_er_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _visits_during_period_2 AS visit
            WHERE (visit.person_id = index_event.person_id)
              AND (visit.visit_type = 'emergency')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

\echo '    calculating covariate (c), `had_inpatient_visit_during_period_2`'

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'had_inpatient_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _visits_during_period_2 AS visit
            WHERE (visit.person_id = index_event.person_id)
              AND (visit.visit_type = 'inpatient')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

DROP TABLE _visits_during_period_2;
