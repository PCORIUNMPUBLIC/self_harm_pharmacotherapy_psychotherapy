-- STEP 4: Calculate outcome variables and covariates

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.variables,design_3'

DROP TABLE IF EXISTS "variables,design_3" CASCADE;
CREATE TABLE "variables,design_3"
  (
        person_id BIGINT NOT NULL,
    variable_name TEXT NOT NULL,
   variable_value TEXT NOT NULL
  );

CREATE INDEX ON "variables,design_3" (person_id, variable_name);

COMMENT ON TABLE "variables,design_3" IS '{{pipeline_version}}';

\echo '  staging events for outcome "variables,design_3"'

\echo '    staging first competing risks'

-- first competing risk occurring for any given patient
CREATE TEMPORARY TABLE _first_competing_risk AS
    SELECT DISTINCT ON (person_id)
           *
      FROM "competing_risks,design_3"
  ORDER BY person_id,
           lower(event_period) ASC;

CREATE INDEX ON _first_competing_risk (person_id);
CREATE INDEX ON _first_competing_risk USING GIST (event_period);
CREATE INDEX ON _first_competing_risk (event_type);

\echo '  calculating covariate #0(a), `type_of_first_competing_risk`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'type_of_first_competing_risk'::TEXT,
         event_type
    FROM _first_competing_risk;

\echo '  calculating covariate #0(b), `metavisit_of_first_competing_risk`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'metavisit_of_first_competing_risk'::TEXT,
         metavisit_occurrence_id
    FROM _first_competing_risk;

\echo '  calculating covariate #1, `age_at_first_day_of_index_visit`'

INSERT INTO "variables,design_3"
  WITH
    _age_at_first_day_of_index_visit AS (
       SELECT index_event.person_id,
              (date_part('year', age(
                lower(index_event.period_2),
                cohort.birth_date))) AS age
         FROM "sequences_of_interest,design_3" AS index_event
         JOIN cohort_of_interest AS cohort USING (person_id)
      )
  SELECT person_id,
         'age_at_first_day_of_index_visit'::TEXT,
         age
    FROM _age_at_first_day_of_index_visit;

\echo '  calculating covariate #2, `sex`'

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'sex'::TEXT,
         cohort.gender
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN cohort_of_interest AS cohort USING (person_id);

\echo '  calculating covariate #3, `injury_during_period_5`'

CREATE TEMPORARY TABLE _injuries_during_period_5 AS
  SELECT person_id
    FROM _first_competing_risk AS competing_risk
    JOIN events_of_interest AS event
        USING (person_id)
        WHERE (competing_risk.event_type != 'censoring_event')
          AND (competing_risk.event_period && event.event_period)
          AND (
                  (event.concept_set_name LIKE
                    'diagnoses;somatic_comorb;injury;%')
               OR (event.concept_set_name LIKE
                    'procedures;procedure;probable_injury;%')
              );

CREATE INDEX ON _injuries_during_period_5 (person_id);

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'injury_during_period_5'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _injuries_during_period_5 AS event
            WHERE (event.person_id = index_event.person_id)
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

DROP TABLE _injuries_during_period_5;

\echo '  calculating covariate #4, `self_harm_during_period_5`'

CREATE TEMPORARY TABLE _self_harm_during_period_5 AS
       SELECT person_id
         FROM _first_competing_risk AS competing_risk
         JOIN events_of_interest AS event
        USING (person_id)
        WHERE (competing_risk.event_type != 'censoring_event')
          AND (competing_risk.event_period && event.event_period)
          AND (event.concept_set_name LIKE
           'diagnoses;mental_comorb;self_harm;%');

CREATE INDEX ON _self_harm_during_period_5 (person_id);

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'self_harm_during_period_5'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _self_harm_during_period_5 AS event
            WHERE (event.person_id = index_event.person_id)
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

DROP TABLE _self_harm_during_period_5;

\echo '  calculating covariate #5, `presence_of_diagnosed_psychosis`'

CREATE TEMPORARY TABLE _psychosis_diagnoses AS
  SELECT person_id
    FROM events_of_interest AS event
   WHERE (
             (event.concept_set_name LIKE
               'diagnoses;mmi;schizophrenia;%')
          OR (event.concept_set_name LIKE
               'diagnoses;mmi;bipolar_spectrum;schizoaffective;%')
          OR (event.concept_set_name LIKE
               'diagnoses;mental_comorb;acute_psychosis;%')
          OR (event.concept_set_name LIKE
               'diagnoses;episode;psychotic;present;%')
         );

CREATE INDEX ON _psychosis_diagnoses (person_id);

INSERT INTO "variables,design_3"
  SELECT index_event.person_id,
         'presence_of_diagnosed_psychosis'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _psychosis_diagnoses AS event
            WHERE (event.person_id = index_event.person_id)
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS index_event;

DROP TABLE _psychosis_diagnoses;

\echo '  calculating covariate #6, `n_visit_days_during_previous_year`'

CREATE TEMPORARY TABLE _n_visit_days_during_previous_year AS
  WITH
    _visits_during_previous_year AS (
       SELECT index_event.person_id,
              (index_event.period_1 * metavisit.metavisit_period) AS visit_period
         FROM "sequences_of_interest,design_3" AS index_event
         JOIN metavisits_of_interest AS metavisit USING (person_id)
        WHERE (index_event.period_1 && metavisit.metavisit_period)
      )
    SELECT person_id,
           codex_period_length(array_agg(visit_period)) AS value
      FROM _visits_during_previous_year
  GROUP BY person_id;

CREATE INDEX ON _n_visit_days_during_previous_year (person_id);

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'n_visit_days_during_previous_year'::TEXT,
            coalesce(variable.value, 0)
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _n_visit_days_during_previous_year AS variable USING (person_id);

DROP TABLE _n_visit_days_during_previous_year;

\echo '  calculating covariate #7, `length_of_index_visit`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'length_of_index_visit'::TEXT,
         codex_period_length(period_2)
    FROM "sequences_of_interest,design_3";

\echo '  calculating covariates #8*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_08.sql'

\echo '  calculating covariates #9*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_09.sql'

\echo '  calculating covariates #10*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_10.sql'

\echo '  calculating covariate #11(a), `mental_procedure_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'mental_procedure_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'procedures;mental_procedure;%');

\echo '  calculating covariate #11(b), `mental_procedure_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'mental_procedure_during_period_2'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'procedures;mental_procedure;%');

\echo '  calculating covariates #12*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_12.sql'

\echo '  calculating covariate #13(a), `injury_related_procedure_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'injury_related_procedure_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'procedures;procedure;probable_injury;%');

\echo '  calculating covariate #13(b), `injury_related_procedure_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'injury_related_procedure_during_period_2'::TEXT,
               (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'procedures;procedure;probable_injury;%');

\echo '  calculating covariate #14(a), `occurrence_of_non_medical_injury_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'occurrence_of_non_medical_injury_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_1)
        AND (event.concept_set_name LIKE
              'diagnoses;somatic_comorb;injury;%');

\echo '  calculating covariate #14(b), `occurrence_of_non_medical_injury_during_period_2`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'occurrence_of_non_medical_injury_during_period_2'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN events_of_interest AS event
         ON (event.person_id = index_event.person_id)
        AND (event.event_period && index_event.period_2)
        AND (event.concept_set_name LIKE
              'diagnoses;somatic_comorb;injury;%');

\echo '  staging events for covariates #15 to #17'

CREATE TEMPORARY TABLE _period_4 AS
  SELECT index_event.person_id,
         codex_period(
           index_event.period_3,
           lower(competing_risk.event_period)) AS period
    FROM _first_competing_risk AS competing_risk
    JOIN "sequences_of_interest,design_3" AS index_event
   USING (person_id);

CREATE INDEX ON _period_4 (person_id);

\echo '  calculating covariate #15, `n_outpatient_visits_during_period_4`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'n_outpatient_visits_during_period_4'::TEXT,
         0
    FROM _period_4;

UPDATE "variables,design_3"
   SET variable_value = variable.variable_value
  FROM (
          SELECT index_event.person_id,
                 'n_outpatient_visits_during_period_4'::TEXT AS variable_name,
                 count(*) AS variable_value
            FROM _period_4 AS index_event
            JOIN visits_of_interest AS visit USING (person_id)
           WHERE (visit.visit_period && index_event.period)
             AND (visit.visit_type = 'outpatient')
        GROUP BY index_event.person_id
       ) AS variable
 WHERE ("variables,design_3".person_id = variable.person_id)
   AND ("variables,design_3".variable_name = variable.variable_name);

\echo '  calculating covariate #16, `n_psychotherapy_procedures_during_period_4`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'n_psychotherapy_procedures_during_period_4'::TEXT,
         0
    FROM _period_4;

UPDATE "variables,design_3"
   SET variable_value = variable.variable_value
  FROM (
          SELECT index_event.person_id,
                 'n_psychotherapy_procedures_during_period_4'::TEXT AS variable_name,
                 count(*) AS variable_value
            FROM _period_4 AS index_event
            JOIN events_of_interest AS event USING (person_id)
           WHERE (event.event_period && index_event.period)
             AND (event.concept_set_name LIKE
                   'procedures;psychotherapy;procedure;%')
        GROUP BY index_event.person_id
       ) AS variable
 WHERE ("variables,design_3".person_id = variable.person_id)
   AND ("variables,design_3".variable_name = variable.variable_name);

\echo '  calculating covariate #17, `n_family_psychotherapy_procedures_during_period_4`'

INSERT INTO "variables,design_3"
  SELECT person_id,
         'n_family_psychotherapy_procedures_during_period_4'::TEXT,
         0
    FROM _period_4;

UPDATE "variables,design_3"
   SET variable_value = variable.variable_value
  FROM (
          SELECT index_event.person_id,
                 'n_family_psychotherapy_procedures_during_period_4'::TEXT AS variable_name,
                 count(*) AS variable_value
            FROM _period_4 AS index_event
            JOIN events_of_interest AS event USING (person_id)
           WHERE (event.event_period && index_event.period)
             AND (event.concept_set_name LIKE
                   'procedures;psychotherapy;procedure;family;%')
        GROUP BY index_event.person_id
       ) AS variable
 WHERE ("variables,design_3".person_id = variable.person_id)
   AND ("variables,design_3".variable_name = variable.variable_name);

DROP TABLE _period_4;

\echo '  calculating covariates #18*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_18.sql'

\echo '  calculating covariates #19*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_19.sql'

\echo '  calculating covariate #20, `was_hospitalized_year_prior_period_3`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'was_hospitalized_year_prior_period_3'::TEXT,
            (metavisit.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN metavisits_of_interest AS metavisit
         ON (metavisit.person_id = index_event.person_id)
        AND (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
        AND (metavisit.metavisit_period && codex_period(
               lower(index_event.period_1),
               index_event.period_3));

\echo '  calculating covariate #21, `was_hospitalized_for_psychiatric_reasons_year_prior_period_3`'

CREATE TEMPORARY TABLE _hospitalizations_for_psychiatric_reasons AS
  SELECT metavisit.person_id,
         metavisit.metavisit_period
    FROM metavisits_of_interest AS metavisit
    JOIN events_of_interest AS event
   USING (metavisit_occurrence_id)
         -- at least one of the visits is inpatient or ER
   WHERE (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
         -- at least one of the events is for a primary diagnosis
         -- or primary procedure associated with mental illness
     AND (event.concept_set_name LIKE
           'diagnoses;psychiatric_diagnoses;%')
     AND (event.concept_ids && ARRAY[
           44786627,  -- 'Primary condition'
           44786630,  -- 'Primary Procedure'
           38000183,  -- 'Inpatient detail - primary' (condition)
           38000258,  -- 'Inpatient detail - primary position' (condition)
           38000248,  -- 'Inpatient detail - primary position' (procedure)
           38000184,  -- 'Inpatient detail - 1st position' (condition)
           38000249,  -- 'Inpatient detail - 1st position' (procedure)
           38000199,  -- 'Inpatient header - primary' (condition)
           38000250,  -- 'Inpatient header - primary position' (procedure)
           38000200,  -- 'Inpatient header - 1st position'(condition)
           38000251,  -- 'Inpatient header - 1st position' (procedure)
           38000266,  -- 'Outpatient detail - primary position' (procedure)
           38000215,  -- 'Outpatient detail - 1st position' (condition)
           38000267,  -- 'Outpatient detail - 1st position' (procedure)
           38000268,  -- 'Outpatient header - primary position' (procedure)
           38000230,  -- 'Outpatient header - 1st position' (condition)
           38000269,  -- 'Outpatient header - 1st position' (procedure)
           45756835,  -- 'Carrier claim header - 1st position' (condition)
           45756843,  -- 'Carrier claim detail - 1st position' (condition)
           45756900   -- 'Carrier claim detail - 1st position' (procedure)
           ]::INTEGER[]);

CREATE INDEX ON _hospitalizations_for_psychiatric_reasons (person_id);
CREATE INDEX ON _hospitalizations_for_psychiatric_reasons USING GIST (metavisit_period);

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'was_hospitalized_for_psychiatric_reasons_year_prior_period_3'::TEXT,
            (metavisit.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _hospitalizations_for_psychiatric_reasons AS metavisit
         ON (metavisit.person_id = index_event.person_id)
            -- the metavisit overlaps with the year before period 3
        AND (metavisit.metavisit_period && codex_period(
               lower(index_event.period_1),
               index_event.period_3));

DROP TABLE _hospitalizations_for_psychiatric_reasons;

\echo '  calculating covariates #22*:'
\include 'b_calculate_covariates_design_3/b4_get_variables.group_22.sql'

/**
Test: each person should have the same set of "variables,design_3"

    SELECT DISTINCT
           array_sort(
             array_distinct(
               array_agg(variable_name)))
      FROM "variables,design_3"
  GROUP BY person_id

Test: each variable should have a value for each person

    SELECT variable_name,
           count(DISTINCT person_id) AS n_persons
      FROM "variables,design_3"
  GROUP BY variable_name
  ORDER BY variable_name
**/

/******************************************************************************/

\echo '* exporting {{work_schema}}.variables,design_3'

\! rm -f {{work_schema}}.variables,design_3.csv.gz

CREATE TEMPORARY VIEW _variables AS
    SELECT *
      FROM "variables,design_3"
  ORDER BY person_id,
           variable_name;

\copy (SELECT * FROM _variables) TO '{{work_schema}}.variables,design_3.csv' WITH CSV HEADER

\! gzip -9 {{work_schema}}.variables,design_3.csv

