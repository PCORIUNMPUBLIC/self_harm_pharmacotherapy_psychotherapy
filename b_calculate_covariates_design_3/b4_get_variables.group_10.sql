
\echo '    staging events'

\echo '      creating _comorbidities_during_period_1_to_3'

DROP TABLE IF EXISTS _comorbidities_during_period_1_to_3 CASCADE;
CREATE TEMPORARY TABLE _comorbidities_during_period_1_to_3 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN  events_of_interest AS event
   USING (person_id)
   WHERE (event.concept_set_name LIKE 'diagnoses;%')
     AND (event.event_period && codex_period(
            lower(index_event.period_1),
            index_event.period_3));

CREATE INDEX ON _comorbidities_during_period_1_to_3 (person_id);
CREATE INDEX ON _comorbidities_during_period_1_to_3 (concept_set_name);
CREATE INDEX ON _comorbidities_during_period_1_to_3 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '  calculating covariate (a)(i), `observation_of_alcohol_dependence_or_abuse_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_alcohol_dependence_or_abuse_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;alcohol_depend_abus;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(ii), `observation_of_alcohol_induced_mental_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_alcohol_induced_mental_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;alcohol_induced;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(iii), `observation_of_drug_induced_mental_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_drug_induced_mental_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;drug_induced;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(iv), `observation_of_drug_dependence_or_abuse_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_drug_dependence_or_abuse_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;drug_depend_abus;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(v), `observation_of_delirium_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_delirium_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;delirium;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(vi), `observation_of_other_mood_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_other_mood_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;other_mood;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(vii), `observation_of_acute_psychosis_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_acute_psychosis_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;acute_psychosis;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(viii), `observation_of_anxiety_and_dissociative_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_anxiety_and_dissociative_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;anxiety_dissoc;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(ix), `observation_of_generalized_anxiety_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_generalized_anxiety_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;gad;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(x), `observation_of_panic_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_panic_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;panic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xi), `observation_of_psychosomatic_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_psychosomatic_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;psychosomat;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xii), `observation_of_personality_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_personality_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;personality;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xiii), `observation_of_sexual_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_sexual_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;sexual;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xiv), `observation_of_motor_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_motor_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;motor;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xv), `observation_of_eating_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_eating_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;eating;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xvi), `observation_of_sleep_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_sleep_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;sleep;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xvii), `observation_of_stress_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_stress_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;stress;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xviii), `observation_of_conduct_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_conduct_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;conduct_impuls;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xix), `observation_of_youth_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_youth_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;child_adolesc;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xx), `observation_of_hyperkinetic_syndrome_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_hyperkinetic_syndrome_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;hyperkinet;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xxi), `observation_of_development_delay_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_development_delay_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;development_delay;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xxii), `observation_of_pregnancy_related_mental_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_pregnancy_related_mental_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;pregnancy_delivery;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (a)(xxiii), `observation_of_intentional_self_harm_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_intentional_self_harm_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;mental_comorb;self_harm;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(i), `observation_of_autoimmune_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_autoimmune_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;autoimmune;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(ii), `observation_of_cardiovascular_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_cardiovascular_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;cardiovascular;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(iii), `observation_of_dermatological_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_dermatological_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;dermatological;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(iv), `observation_of_endocrinopathy_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_endocrinopathy_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;endocrinopathy;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(v), `observation_of_obesity_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_obesity_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;obesity_and_overweight;obesity;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(vi), `observation_of_hypertension_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_hypertension_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;hypertension;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(vii), `observation_of_cardiac_arrhythmia_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_cardiac_arrhytmia_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;arrhytmia;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(viii), `observation_of_kidney_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_kidney_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;kidney;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(ix), `observation_of_metabolic_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_metabolic_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;metabolic;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(x), `observation_of_musculoskeletal_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_musculoskeletal_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;musculoskeletal;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xi), `observation_of_nervous_system_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_nervous_system_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;nerv_system;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xii), `observation_of_cns_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_cns_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;central_nerv_system;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xiii), `observation_of_seizure_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_seizure_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;seizure;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xiv), `observation_of_demyelinating_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_demyelinating_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;demyelinat;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xv), `observation_of_neuromuscular_transmission_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_neuromuscular_transmission_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;neuromusc_transmission;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xvi), `observation_of_chronic_pain_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_chronic_pain_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;chronic_pain;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xvii), `observation_of_movement_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_movement_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;movement_medicat;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xviii), `observation_of_pulmonary_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_pulmonary_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;pulmon;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xix), `observation_of_thyroid_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_thyroid_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;thyroid;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xx), `observation_of_liver_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_liver_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;liver;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxi), `observation_of_digestive_tract_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_digestive_tract_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;digestive;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxii), `observation_of_hiv_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_hiv_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;hiv_aids;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxiii), `observation_of_tuberculosis_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_tuberculosis_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;tuberculosis;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxiv), `observation_of_pregnancy_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_pregnancy_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;currently_pregnant;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxv), `observation_of_delivery_and_perinatal_care_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_delivery_and_perinatal_care_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;delivery_perinatal_care;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxvi), `observation_of_abortion_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_abortion_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;abortion;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxvii), `observation_of_chronic_infectious_disease_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_chronic_infectious_disease_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;chronic_infect_disease;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxviii), `observation_of_neoplasm_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_neoplasm_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;neoplasm_hamartoma;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxix), `observation_of_poststreptococcal_disorder_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_poststreptococcal_disorder_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;post_streptococ;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxx), `observation_of_rheumatism_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_rheumatism_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;rheumatism;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

\echo '  calculating covariate (b)(xxxi), `observation_of_std_during_period_1_to_3`'

INSERT INTO "variables,design_3"
  SELECT op_start.person_id,
         'observation_of_std_during_period_1_to_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_to_3 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'diagnoses;somatic_comorb;sexually_transmitted;%')
           ))::INTEGER
    FROM "sequences_of_interest,design_3" AS op_start;

/**
Test: for any patient_id, each of these covariates should have a value

    SELECT variable_name,
           count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
           count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
           count(*) AS n_total
      FROM "variables,design_3"
     WHERE (variable_name LIKE 'observation_of_%_during_period_1_to_3')
  GROUP BY variable_name
  ORDER BY variable_name;

**/
