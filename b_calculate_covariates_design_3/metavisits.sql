SET search_path TO {{work_schema}};

\copy (select metavisit_occurrence_id, person_id, metavisit_period from metavisits_of_interest where visit_types && ARRAY['inpatient','emergency']) to '{{work_schema}}_metavisits.csv' with csv;
