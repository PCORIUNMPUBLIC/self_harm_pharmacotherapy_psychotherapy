
SET search_path TO {{work_schema}};

-- create a file with all person_ids, event_periods and concept_set_names for the 4 different major mental illnesses
create temporary table bob as (
	  SELECT person_id, event_period, concept_set_name
	    FROM {{work_schema}}.events_of_interest
	   WHERE concept_set_name LIKE 'diagnoses;mmi;bipolar_spectrum;major_depressive%'
		  OR concept_set_name LIKE 'diagnoses;mmi;schizophrenia%'
		  OR concept_set_name LIKE 'diagnoses;mmi;bipolar_spectrum;schizoaffective%'
		  OR concept_set_name LIKE 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder%'
	 ORDER BY person_id
);
\copy bob to '{{work_schema}}_mmi.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';	
