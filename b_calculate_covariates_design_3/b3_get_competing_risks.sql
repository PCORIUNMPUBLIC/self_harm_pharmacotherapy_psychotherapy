-- STEP 3: List competing risks

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.competing_risks,design_3'

DROP TABLE IF EXISTS "competing_risks,design_3" CASCADE;
CREATE TABLE "competing_risks,design_3"
  (
                 person_id BIGINT NOT NULL,
              event_period DATERANGE NOT NULL,
                event_type TEXT NOT NULL,
   metavisit_occurrence_id TEXT NOT NULL
  );

COMMENT ON TABLE "competing_risks,design_3" IS '{{pipeline_version}}';

\echo '  listing competing risk #1, `hospitalization`'

INSERT INTO "competing_risks,design_3"
  WITH
    _first_hospitalization AS (
         SELECT DISTINCT ON (op_start.person_id)
                op_start.person_id,
                metavisit.metavisit_period,
                metavisit.metavisit_occurrence_id
           FROM metavisits_of_interest AS metavisit
           JOIN "sequences_of_interest,design_3" AS op_start
          USING (person_id)
          WHERE (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
            AND (lower(metavisit.metavisit_period) > op_start.period_3)
       ORDER BY op_start.person_id,
                metavisit.metavisit_period
      )
  SELECT person_id,
         metavisit_period,
         'hospitalization'::TEXT,
         metavisit_occurrence_id
    FROM _first_hospitalization;

\echo '  listing competing risk #2, `censoring_event`'

\echo '    death'

INSERT INTO "competing_risks,design_3"
  SELECT op_start.person_id,
         codex_instant(event.death_date),
         'censoring_event'::TEXT,
         ''::TEXT
    FROM "sequences_of_interest,design_3" AS op_start
    JOIN {{cdm_schema}}.death AS event USING (person_id);

\echo '    last recorded event'

INSERT INTO "competing_risks,design_3"
  WITH
    _last_recorded_events AS (
         SELECT DISTINCT ON (op_start.person_id)
                op_start.person_id,
                event.event_period,
                event.metavisit_occurrence_id
           FROM "sequences_of_interest,design_3" AS op_start
           JOIN events_of_interest AS event USING (person_id)
       ORDER BY op_start.person_id,
                upper(event.event_period) DESC
      )
    SELECT person_id,
           codex_instant(upper(event_period)),
           'censoring_event'::TEXT,
           coalesce(metavisit_occurrence_id, '')
      FROM _last_recorded_events AS event;

\echo '    onset of excluded conditions or drugs'

INSERT INTO "competing_risks,design_3"
  SELECT op_start.person_id,
         event.event_period,
         'censoring_event'::TEXT,
         coalesce(event.metavisit_occurrence_id, '')
    FROM "sequences_of_interest,design_3" AS op_start
    JOIN events_of_interest AS event USING (person_id)
   WHERE (event.concept_set_name LIKE 'diagnoses;exclusion;intellectual_disability;%')
      OR (event.concept_set_name LIKE 'diagnoses;exclusion;autism_spectrum;%')
      OR (event.concept_set_name LIKE 'diagnoses;exclusion;organic_disorder;%')
      OR (event.concept_set_name LIKE 'diagnoses;exclusion;parkinson_disease;%')
      OR (event.concept_set_name LIKE 'drugs;exclusion;memantine;%')
      OR (event.concept_set_name LIKE 'drugs;exclusion;cholinesterase_inhibitor;%');

\echo '  indexing competing_risks,design_3'

\echo '    creating index on competing_risks,design_3.person_id'

CREATE INDEX ON "competing_risks,design_3" (person_id);

\echo '    creating index on competing_risks,design_3.event_period'

CREATE INDEX ON "competing_risks,design_3" USING GIST (event_period);

\echo '    creating index on competing_risks,design_3.event_type'

CREATE INDEX ON "competing_risks,design_3" (event_type);

\echo '  truncating competing_risks,design_3'

-- delete all competing risks that are not from patients in
-- `sequences_of_interest,design_3` (this should not be necessary)
DELETE
  FROM "competing_risks,design_3" AS op_stop
 USING (
           SELECT DISTINCT
                  op_stop.person_id
             FROM "competing_risks,design_3" AS op_stop
        LEFT JOIN "sequences_of_interest,design_3" AS op_start
            USING (person_id)
            WHERE (op_start.person_id IS NULL)
       ) AS orphan
 WHERE (op_stop.person_id = orphan.person_id);

-- delete all competing risks that started
-- on or before the index prescription date
DELETE
  FROM "competing_risks,design_3" AS op_stop
 USING "sequences_of_interest,design_3" AS op_start
 WHERE (op_start.person_id = op_stop.person_id)
   AND (lower(op_stop.event_period)
         <= op_start.period_3);

-- adding a 'censoring_event' competing risk for
-- all person_id that have no competing risk
INSERT INTO "competing_risks,design_3"
  WITH
    _people_without_competing_risk AS (
          SELECT sequence.person_id,
                 sequence.period_3
            FROM "sequences_of_interest,design_3" AS sequence
       LEFT JOIN "competing_risks,design_3" AS competing_risk
           USING (person_id)
           WHERE (competing_risk.person_id IS NULL)
      )
  SELECT person_id,
         codex_instant(period_3 + 1),
         'censoring_event'::TEXT,
         ''::TEXT
    FROM _people_without_competing_risk;

\echo '    analyzing competing_risks,design_3'

ANALYZE "competing_risks,design_3";

/**
Test: each competing risk should have been found at least once

    SELECT event_type,
           count(DISTINCT person_id) AS n_persons
      FROM "competing_risks,design_3"
  GROUP BY event_type
  ORDER BY event_type;

**/
