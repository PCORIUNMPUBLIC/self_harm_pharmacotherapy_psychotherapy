
\echo '    staging events'

\echo '      creating _mmi_during_period_1'

DROP TABLE IF EXISTS _mmi_during_period_1 CASCADE;
CREATE TEMPORARY TABLE _mmi_during_period_1 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM "sequences_of_interest,design_3" AS index_event
    JOIN events_of_interest AS event
   USING (person_id)
   WHERE (event.concept_set_name LIKE 'diagnoses;mmi;%')
     AND (event.event_period && index_event.period_1);

CREATE INDEX ON _mmi_during_period_1 (person_id);
CREATE INDEX ON _mmi_during_period_1 (concept_set_name);
CREATE INDEX ON _mmi_during_period_1 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate (a), `bipolar_disorder_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'bipolar_disorder_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _mmi_during_period_1 AS event
         ON (event.person_id = index_event.person_id)
        AND (event.concept_set_name LIKE
              'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;%');

\echo '    calculating covariate (b), `mdd_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'mdd_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _mmi_during_period_1 AS event
         ON (event.person_id = index_event.person_id)
        AND (event.concept_set_name LIKE
              'diagnoses;mmi;bipolar_spectrum;major_depressive;%');

\echo '    calculating covariate (c), `sad_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'sad_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _mmi_during_period_1 AS event
         ON (event.person_id = index_event.person_id)
        AND (event.concept_set_name LIKE
              'diagnoses;mmi;bipolar_spectrum;schizoaffective;%');

\echo '    calculating covariate (d), `schizophrenia_during_period_1`'

INSERT INTO "variables,design_3"
     SELECT index_event.person_id,
            'schizophrenia_during_period_1'::TEXT,
            (event.person_id IS NOT NULL)::INTEGER
       FROM "sequences_of_interest,design_3" AS index_event
  LEFT JOIN _mmi_during_period_1 AS event
         ON (event.person_id = index_event.person_id)
        AND (event.concept_set_name LIKE
              'diagnoses;mmi;schizophrenia;%');

DROP TABLE _mmi_during_period_1;
