-- STEP 2: Identify sequences of events of interest

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.sequences_of_interest,design_3'

\echo '  staging events'

\echo '    staging MMI-related metavisits'

CREATE TEMPORARY TABLE "_mmi_related_metavisits,design_3" AS
  SELECT person_id,
         metavisit_period AS event_period
    FROM metavisits_of_interest AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'diagnoses;mmi;%')
         ));

COMMENT ON TABLE "_mmi_related_metavisits,design_3" IS '{{pipeline_version}}';

CREATE INDEX ON "_mmi_related_metavisits,design_3" (person_id);
CREATE INDEX ON "_mmi_related_metavisits,design_3" USING GIST (event_period);

\echo '    staging unwanted events before index prescription (periods 0-3)'

CREATE TEMPORARY TABLE _unwanted_events_before_index_prescription AS
  SELECT person_id,
         event_period
    FROM events_of_interest
   WHERE (concept_set_name LIKE 'diagnoses;exclusion;intellectual_disability;%')
      OR (concept_set_name LIKE 'diagnoses;exclusion;autism_spectrum;%')
      OR (concept_set_name LIKE 'diagnoses;exclusion;organic_disorder;%')
      OR (concept_set_name LIKE 'diagnoses;exclusion;parkinson_disease;%')
      OR (concept_set_name LIKE 'drugs;exclusion;memantine;%')
      OR (concept_set_name LIKE 'drugs;exclusion;cholinesterase_inhibitor;%');

CREATE INDEX ON _unwanted_events_before_index_prescription (person_id);
CREATE INDEX ON _unwanted_events_before_index_prescription USING GIST (event_period);

/******************************************************************************/

\echo '  finding first recorded date'

CREATE TEMPORARY TABLE _first_recorded_date AS
    SELECT lower(event_period) AS date
      FROM events_of_interest
  ORDER BY event_period
     LIMIT 1;

\echo '  finding sequences of interest'

DROP TABLE IF EXISTS "sequences_of_interest,design_3";
CREATE TABLE "sequences_of_interest,design_3" AS
    SELECT DISTINCT ON (index_event.person_id)  -- we only keep the first
           index_event.*                        -- index_event per person
      FROM (
            SELECT cohort.person_id,
                   -- period 1: One year prior to index visit
                   codex_period(
                     lower(index_event.event_period) - 365,
                     lower(index_event.event_period) - 1
                     ) AS period_1,
                   -- period 2: index visit
                   index_event.event_period AS period_2,
                   -- period 3: index exposure (last day of index visit)
                   (upper(index_event.event_period) - 1
                     ) AS period_3
              FROM cohort_of_interest AS cohort,
                   "_mmi_related_metavisits,design_3" AS index_event,
                   _first_recorded_date AS first_recorded_date
             WHERE (cohort.person_id = index_event.person_id)
                   -- we want the index_event to happen
                   -- after at least one year of known events
               AND ((lower(index_event.event_period)
                     - first_recorded_date.date) >= 365)
                   -- we want the index_event to happen
                   -- between 11 and 65 years of age
--               AND (date_part('year', age(
--                     lower(index_event.event_period),
--                     cohort.birth_date)) BETWEEN 18 AND 65)
           ) AS index_event
           -- we do not want some events prior to the index prescription date
     WHERE (NOT EXISTS (
            SELECT true
              FROM _unwanted_events_before_index_prescription AS event
             WHERE (event.person_id = index_event.person_id)
               AND (lower(event.event_period) <= index_event.period_3)
           ))
  ORDER BY index_event.person_id,
           index_event.period_2;

COMMENT ON TABLE "sequences_of_interest,design_3" IS '{{pipeline_version}}';

\echo '  indexing sequences_of_interest,design_3'

ALTER TABLE "sequences_of_interest,design_3"
  ADD PRIMARY KEY (person_id);

CREATE INDEX ON "sequences_of_interest,design_3" USING GIST (period_1);
CREATE INDEX ON "sequences_of_interest,design_3" USING GIST (period_2);
CREATE INDEX ON "sequences_of_interest,design_3" (period_3);

ANALYZE "sequences_of_interest,design_3";
