
-- depressive episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[3-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F32.[0-3]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[012389]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F25.1');

-- depressive episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.2[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.3[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.5[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.82');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

-- CPT4 depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756912);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106326);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101900);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757095);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

-- HCPCS depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617572);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617866);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533366);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533229);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786417);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786418);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786437);

-- manic episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[1289]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-2]%');

-- manic episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.0[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.1[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.4[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.81');

-- mixed episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.6%');

-- mixed episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.6[0-4]');

-- unknown polarity episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[89]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F25');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F25.[089]');

-- unknown polarity episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.80');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '295.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.7[0-4]');

-- no psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.3%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.6[123]');

-- no psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0123456][123]');

-- CPT4 Major depressive disorder, severe without psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

-- psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '295.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.7[0-4]');

-- CPT4 psychotic depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[89]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[8-9]');

-- unknown psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0123456]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- mild episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.61');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.0');

-- mild episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]1');

-- CPT4 mild depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

-- moderate episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.62');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.1');

-- moderate episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]2');

-- CPT4 moderate depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- severe episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.63');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F32.[2-3]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[2-3]');

-- severe episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][3-4]');

-- CPT4 severe depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown severity episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[89]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- unknown severity episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01234567]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '295.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.7[0-4]');

-- Autism-spectrum disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F84%');

-- Autism-spectrum disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '299%');

-- Delusional disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F22%');

-- Delusional disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.8');

-- Intellectual disabilities (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F70%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F71%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F72%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F73%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F78%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F79%');

-- Intellectual disabilities (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '317%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '318%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '319%');

-- Organic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F01%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F02%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F03%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F04%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F06%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F07%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F09%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F48.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'G30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F19.9[67]');

-- Organic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '290%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '294%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '293.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '310%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '291.[12]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '292.8[23]');

-- Parkinson's disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G20');

-- Parkinson's disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '332.0');

-- Schizophrenia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F20%');

-- Schizophrenia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.[012345689]');

-- Acute psychoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

-- Acute psychoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

-- Alcohol dependence or abuse (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

-- Alcohol dependence or abuse (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '303%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '305.0%');

-- Alcohol-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.9%');

-- Alcohol-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '291%');

-- Anxiety and dissociative disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F40%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F41%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F42%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F44%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F48%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F55%');

-- Anxiety and dissociative disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '300.[0123569]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.2');

-- NO_NAME (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.9');

-- NO_NAME (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.09');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F92%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F93%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F94%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F98%');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '313%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[67]');

-- Conduct disorders and impulse disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F91%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F63%');

-- Conduct disorders and impulse disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '312%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '313.81');

-- Delirium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F05');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.931');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.931');

-- Delirium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '291.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '292.81');

-- Specific delays in development: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F80%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F81%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F82%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F88%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F89%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R48.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'H93.25');

-- Specific delays in development: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '315%');

-- Drug dependence or abuse (except alcohol) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[12]%');

-- Drug dependence or abuse (except alcohol) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '305.[1-9]%');

-- Drug-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[129]%');

-- Drug-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '292%');

-- Eating disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F50%');

-- Eating disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '307.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.5%');

-- Generalized anxiety disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.1');

-- Generalized anxiety disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.02');

-- Hyperkinetic syndrome of childhood: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F90%');

-- Hyperkinetic syndrome of childhood: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '314%');

-- Motor disorders (tics) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F95%');

-- Motor disorders (tics) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[023]%');

-- Other and unspecified mood disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F39');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.81');

-- Other and unspecified mood disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.9%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '301.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '625.4');

-- Panic disorder without agoraphobia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.0');

-- Panic disorder without agoraphobia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.01');

-- Personality disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F21%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F60%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F69%');

-- Personality disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '301.[023456789]%');

-- Pregnancy and delivery related mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O90.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'O99.34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F53');

-- Pregnancy and delivery related mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '648.4%');

-- Psychosomatic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F42.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F45%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F52.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F54%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F59%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G44.209');

-- Psychosomatic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '306%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '300.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '316%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.8%');

-- Intentional self-harm (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X7[1-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X8[0-3]%');

-- Intentional self-harm (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E95[0-9]%');

-- Intentional self-harm (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244894);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(439235);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4303690);

-- Sexual and gender identity disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F52%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F64%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F65%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F66%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z87.890');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R37');

-- Sexual and gender identity disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '302%');

-- Sleep disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F51%');

-- Sleep disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.4%');

-- Sleep disorders (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(435524);

-- Acute reaction to stress and adjustment disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F43%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R45.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F93.0');

-- Acute reaction to stress and adjustment disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '308%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '309%');

-- bipolar disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31%');

-- bipolar disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.4%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.6%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- bipolar disorder, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[289]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-6]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[89]%');

-- bipolar disorder, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.0[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.1[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.4[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.5[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.6[0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- bipolar I, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[129]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-6]%');

-- bipolar I, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01456][0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

-- bipolar II, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.81');

-- bipolar II, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

-- bipolar NOS, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

-- bipolar NOS, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;no_remission;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- MDD (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F32%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33%');

-- MDD (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[23]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

-- MDD, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F32.[0-3]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[012389]');

-- MDD, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[23][0-4]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

-- SAD (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- SAD (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '295.7%');

-- SAD, no remission codes exist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;schizoaffective;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- SAD, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;schizoaffective;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.7[0-4]');

-- Schizophrenia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F20%');

-- Schizophrenia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.[012345689]%');

-- NO_NAME (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;schizophrenia;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F20');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;schizophrenia;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F20.[012389]%');

-- NO_NAME (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;schizophrenia;no_remission;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.[01234589]%');

-- all psychiatric diagnoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F%');

-- all psychiatric diagnoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '29%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '31%');

-- pregnancy with abortive outcome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;abortion;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40539858);

-- cardiac arrhythmia (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;arrhytmia;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44784217);

-- autoimmune (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;autoimmune;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434621);

-- cardiovascular (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;cardiovascular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(134057);

-- central nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;central_nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376106);

-- chronic infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;chronic_infect_disease;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4073287);

-- chronic pain (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;chronic_pain;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436096);

-- patient currently pregnant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;currently_pregnant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4299535);

-- delivery and perinatal care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4038495);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4237498);

-- demyelinating disease of CNS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;demyelinat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(375801);

-- dermatological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;dermatological;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4317258);

-- disorder of digestive tract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;digestive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4309188);

-- endocrinopathy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;endocrinopathy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(31821);

-- HIV/AIDS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4241530);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4221489);

-- hypertension (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hypertension;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(316866);

-- NO_NAME (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'S%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'T%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'V%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'W%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[9][2-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Y[2-3]%');

-- NO_NAME (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '8[0-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '9[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '99[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[0-4]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[6-9]%');

-- kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;kidney;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(198124);

-- liver disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;liver;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(194984);

-- metabolic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;metabolic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436670);

-- medication-induced movement disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;movement_medicat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4186461);

-- musculoskeletal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;musculoskeletal;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244662);

-- neoplasm and/or hamartoma (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;neoplasm_hamartoma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4266186);

-- nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376337);

-- disorder of neuromuscular transmission (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;neuromusc_transmission;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4103776);

-- obesity (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(433736);

-- post-streptococcal disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;post_streptococ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4051204);

-- pulmonary (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;pulmon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(257907);

-- rheumatism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;rheumatism;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(138845);

-- seizure disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;seizure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4029498);

-- sexually transmitted infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;sexually_transmitted;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(440647);

-- thyroidism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(141253);

-- tuberculosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;tuberculosis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434557);

-- Urinary incontinence of non-organic origin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;urinary_incontinence;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4172646);
