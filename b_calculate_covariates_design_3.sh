#!/bin/bash
set -o errexit

steps=(
    "b_calculate_covariates_design_3/b1_declare_therapies.sql"
    "b_calculate_covariates_design_3/b2_get_sequences_of_interest.sql"
    "b_calculate_covariates_design_3/b3_get_competing_risks.sql"
    "b_calculate_covariates_design_3/b4_get_variables.sql"
    "b_calculate_covariates_design_3/b5_get_ingredient_exposures.sql"
    "b_calculate_covariates_design_3/b5_get_therapy_eras.sql"
    "b_calculate_covariates_design_3/metavisits.sql"
    "b_calculate_covariates_design_3/mmi.sql"
    "b_calculate_covariates_design_3/psychotherapy.sql"
    )

for step in "${steps[@]}"; do
    ./psql_run ${step} "$@"; done

echo "metavisit_of_first_competing_risk,person_id,mv_period_start_date,mv_period_end_date" > $2_metavisits1.csv
tr -d "\"\[\)" < $2_metavisits.csv >> $2_metavisits1.csv
gzip -9 $2_metavisits.csv
gzip -9 $2_metavisits1.csv
gzip -9 $2_psychotherapy.tsv
gzip -9 $2_mmi.tsv
python b_calculate_covariates_design_3/format_mmi_file.py $2_mmi.tsv.gz $2_mmi1.tsv.gz
zcat $2_psychotherapy.tsv.gz | gawk 'BEGIN{FS="\t";getline;print "person_id\tdate_psychotherapy\tmetavisit_occurrence_id"}{split($3,a,","); print $1 "\t" substr(a[1],2) "\t" $4}' | gzip -9 > $2_psychotherapy1.tsv.gz

b_calculate_covariates_design_3/b5_get_ingredient_exposures.py \
    30 \
    $2.ingredient_exposures.csv.gz \
    $2.hospitalizations.csv.gz \
    $2.boundaries.csv.gz > $2.ingredient_eras.csv

sort -n $2.ingredient_eras.csv > crud
mv crud $2.ingredient_eras.csv
gzip -9 $2.ingredient_eras.csv

b_calculate_covariates_design_3/transform_eras.py $2.ingredient_eras.csv.gz $2.variables,design_3.csv.gz > $2.wide.eras.csv
gzip -9 $2.wide.eras.csv
