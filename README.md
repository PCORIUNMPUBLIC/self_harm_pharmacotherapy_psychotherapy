# Self Harm Pharmacotherapy Psychotherapy

Comparative effectiveness of pharmacotherapy and psychotherapy for risk of self-harm.

# Introduction

We describe how to generate the data from the paper "Drug-dependent risk of self-harm in patients with bipolar disorder: a comparative effectiveness study using machine learning imputed outcomes". This workflow assumes you have an OMOP 5.0.1 instance of the MarketScan CCAE and MDCR datasets. Later versions of OMOP will probably work, but have not been tested.

## Step 0 create a view to unite CCAE and MDCR

 
Assuming your two schemas are ccae2003_2016 and mdcr2003_2016, the following command will create a new schema, ccaemdcdr2003_2016 comprising a bunch of views on the two schema, which enables us to attribute time periods over both MDCR and CCAE for people with the same person_id. All of the code also assumes that the concept tables are stored within the same table as the data. Our ETLs used the same vocabulary files for CCAE and MDCR. If you were running this code on a single OMOP database schema, you would not need to do this. The resulting schema has the same OMOP tables as the original two schema.

```
./0_combine_cdm_schemas.sh ccae2003_2016 mdcr2003_2016 ccaemdcr2003_2016
```


## Step 1 create an empty schema for forthcoming queries
Assuming your database name is "truven", run the following command line:
```
psql -d truven -c "create schema ccaemdcr2003_2016_selfharm;"

```

## Step 2 run code to declare functions, define cohort, get visits, and get events
The following command will define a cohort of people with major mental illness and create various tables in the new ccaemdcr2003_2016_selfharm schema.
```
./a_create_building_blocks.sh ccaemdcr2003_2016 ccaemdcr2003_2016_selfharm

```

## Step 3 run code to calculate all of the covariates according to the study design
```
./b_calculate_covariates_design_3.sh ccaemdcr2003_2016 ccaemdcr2003_2016_selfharm

```

Several intermediate files are created, but the ones required for the next stage of analysis are:

  - ccaemdcr2003_2016_selfharm.wide.eras.csv.gz
  - ccaemdcr2003_2016_selfharm_metavisits1.csv.gz
  - ccaemdcr2003_2016_selfharm_psychotherapy1.tsv.gz
  - ccaemdcr2003_2016_selfharm_mmi1.tsv.gz

## Step 4 run machine learning code to determine metavisit self-harm probabilities

```
./machine_learning.sh ccaemdcr2003_2016 ccaemdcr2003_2016_selfharm

```

Intermediate files will be created within the machine_learning folder. The final resulting file of probabilities of self-harm within metavisits is used within the statistical analysis:
  - ccaemdcr2003_2016_selfharm_metavisits_probs.tsv.gz


## Step 5 perform statistical analysis

There is an R Sweave file, bipolar_selfharm.Rnw which should be loaded into RStudio:

```
rstudio bipolar_selfharm.Rnw

```
It is probably best to run a block at a time to ensure there are no errors. The first block sets some constants and installs needed R packages. The second block loads in all of the data, doing appropriate merges and joins. The remaining blocks do the analyses.

If all blocks run run successfully you could run again to create a PDF. Setting the doCalcs variable to FALSE will allow you to just create the pdf without rerunning all of the analyses based on saved files from the prior successful run.

It is likely that on a different dataset that some of the drug exposures will be absent, and you will need to comment out the code related to the creation of variables for those exposures.
