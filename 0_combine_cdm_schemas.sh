#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

# Create a composite CDM schema out of two other schemas; the resulting composite
# schema can then be processed like any other CDM schema by the rest of the pipeline
# Syntax:  ./0_combine_cdm_schemas.sh <cdm_schema1> <cdm_schema2> <final_schema>
# Example: ./0_combine_cdm_schemas.sh ccae2003_2016 mdcr2003_2016 ccae_mcdr

_error() { printf "\e[1;4merror\e[24m: $1\e[0m\n" >&2; exit 1; }

if [[ $# -ne 3 ]]; then
    _error "syntax: $(basename $0) <cdm_schema1> <cdm_schema2> <final_schema>"; fi

src=$(basename $0 .sh).sql

(cat <<HEADER && cat ${src}) |
\set QUIET on
\set ON_ERROR_STOP on

SET search_path TO {{work_schema}};
SET client_min_messages TO WARNING;
HEADER
sed \
    -e "s/{{cdm_schema1}}/$1/g" \
    -e "s/{{cdm_schema2}}/$2/g" \
    -e "s/{{work_schema}}/$3/g" \
    | \
psql \
    --no-psqlrc --file - \
    --set "cdm_schema1=$1" \
    --set "cdm_schema2=$1" \
    --set "work_schema=$3"
