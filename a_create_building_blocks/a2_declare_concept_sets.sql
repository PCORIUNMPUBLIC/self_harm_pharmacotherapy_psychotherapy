-- STEP 2 OF 5: Declaration of concept sets of interests for the study

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.concept_sets_of_interest'

DROP TABLE IF EXISTS concept_sets_of_interest CASCADE;
CREATE TABLE concept_sets_of_interest
  (
   concept_set_name VARCHAR(255),
         concept_id INTEGER,
       concept_name VARCHAR(255)
  );

COMMENT ON TABLE concept_sets_of_interest IS '{{pipeline_version}}';

\echo '  importing drug concept sets from {{cdm_schema}}'
\include 'concept_sets/drug_concepts_of_interest.sql'

\echo '  importing condition concept sets from {{cdm_schema}}'
\include 'concept_sets/condition_concepts_of_interest.sql'

\echo '  importing procedure concept sets from {{cdm_schema}}'
\include 'concept_sets/procedure_concepts_of_interest.sql'

\echo '* indexing {{work_schema}}.concept_sets_of_interest'

CREATE INDEX ON concept_sets_of_interest (concept_set_name);
CREATE INDEX ON concept_sets_of_interest (concept_set_name varchar_pattern_ops);
CREATE INDEX ON concept_sets_of_interest (concept_id);

\echo '  removing duplicates'

DELETE
  FROM concept_sets_of_interest AS t1
 USING concept_sets_of_interest AS t2
 WHERE (t1.concept_id = t2.concept_id)
   AND (t1.concept_set_name = t2.concept_set_name)
   AND (t1.ctid < t2.ctid);
