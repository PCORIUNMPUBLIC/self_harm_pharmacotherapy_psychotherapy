#!/bin/bash

echo -e "\nRunning SQL to generate metavisit file"
./psql_run machine_learning/get_metavisits.sql "$@"
if [ $? -eq 0 ]; then
    echo "SQL finished"
else
    echo "SQL failed"
    exit 1
fi

echo -e "\nGenerating ML data"
python3 machine_learning/find_metavisit_covariates.py $2
if [ $? -eq 0 ]; then
    echo "ML data ready"
else
    echo "ML data generation failed"
    exit 3
fi

echo -e "\nRunning ML classification"
python3 machine_learning/classify_metavisits_all.py $2
if [ $? -eq 0 ]; then
    echo "ML classification successful"
else
    echo "ML classification failed"
    exit 4
fi

