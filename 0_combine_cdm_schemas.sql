-- Create a composite CDM schema out of two other CDM schemas
-- The composite can be directly used by the rest of the pipeline

CREATE SCHEMA IF NOT EXISTS {{work_schema}};
SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.concept'

-- we assume the 'concept' and 'concept_ancestors'
-- tables are the same across the two CDM schemas
DROP VIEW IF EXISTS concept CASCADE;
CREATE VIEW concept AS
  SELECT * FROM {{cdm_schema1}}.concept;

\echo '* creating {{work_schema}}.concept_ancestor'

DROP VIEW IF EXISTS concept_ancestor CASCADE;
CREATE VIEW concept_ancestor AS
  SELECT * FROM {{cdm_schema1}}.concept_ancestor;

\echo '* creating {{work_schema}}.concept_relationship'

DROP VIEW IF EXISTS concept_relationship CASCADE;
CREATE VIEW concept_relationship AS
  SELECT * FROM {{cdm_schema1}}.concept_relationship;

\echo '* creating {{work_schema}}.person'

-- the 'person' tables from both CDM schemas overlap; however building
-- a 'UNION' of both (which would remove the duplicates) is a very costly
-- operation. Better do a simpler 'UNION ALL' (which keeps duplicates)
-- then remove duplicates downstream, when building 'cohort_of_interest'
DROP VIEW IF EXISTS person;
CREATE VIEW person AS
     SELECT * FROM {{cdm_schema1}}.person
  UNION ALL
     SELECT * FROM {{cdm_schema2}}.person;

\echo '* creating {{work_schema}}.visit_occurrence'

-- the identifiers in the various event tables, unique within their
-- schema, are not unique across the two CDM schemas; we make them
-- unique again by adding the name of the source schema as a field
DROP VIEW IF EXISTS visit_occurrence CASCADE;
CREATE VIEW visit_occurrence AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.visit_occurrence
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.visit_occurrence;

\echo '* creating {{work_schema}}.condition_occurrence'

DROP VIEW IF EXISTS condition_occurrence CASCADE;
CREATE VIEW condition_occurrence AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.condition_occurrence
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.condition_occurrence;

\echo '* creating {{work_schema}}.drug_exposure'

DROP VIEW IF EXISTS drug_exposure CASCADE;
CREATE VIEW drug_exposure AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.drug_exposure
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.drug_exposure;

\echo '* creating {{work_schema}}.drug_strength'

-- we assume the 'drug_strength' table
-- is the same across both schemas
DROP VIEW IF EXISTS drug_strength CASCADE;
CREATE VIEW drug_strength AS
  SELECT * FROM {{cdm_schema1}}.drug_strength;

\echo '* creating {{work_schema}}.procedure_occurrence'

DROP VIEW IF EXISTS procedure_occurrence CASCADE;
CREATE VIEW procedure_occurrence AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.procedure_occurrence
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.procedure_occurrence;

\echo '* creating {{work_schema}}.observation'

DROP VIEW IF EXISTS observation CASCADE;
CREATE VIEW observation AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.observation
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.observation;

\echo '* creating {{work_schema}}.measurement'

DROP VIEW IF EXISTS measurement CASCADE;
CREATE VIEW measurement AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.measurement
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.measurement;

\echo '* creating {{work_schema}}.device_exposure'

DROP VIEW IF EXISTS device_exposure CASCADE;
CREATE VIEW device_exposure AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.device_exposure
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.device_exposure;

\echo '* creating {{work_schema}}.death'

DROP VIEW IF EXISTS death CASCADE;
CREATE VIEW death AS
     SELECT '{{cdm_schema1}}' AS source_schema, *
       FROM {{cdm_schema1}}.death
  UNION ALL
     SELECT '{{cdm_schema2}}' AS source_schema, *
       FROM {{cdm_schema2}}.death;
